import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { AllcampaignsComponent } from './pages/allcampaigns/allcampaigns.component';
import { ProductdetailComponent } from './pages/productdetail/productdetail.component';
import { CartpageComponent } from './pages/cartpage/cartpage.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { VerficationComponent } from './pages/verfication/verfication.component';
import { ForgetpasswordComponent } from './pages/forgetpassword/forgetpassword.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { PaynowComponent } from './pages/paynow/paynow.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { OrderdetailComponent } from './pages/orderdetail/orderdetail.component';
import { OtpComponent } from './otp/otp.component';
import { ProductdetailticketComponent } from './productdetailticket/productdetailticket.component';
import { ContactusComponent } from './contactus/contactus.component';
import { FaqComponent } from './faq/faq.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TermsconditionComponent } from './termscondition/termscondition.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { HowweworkComponent } from './howwework/howwework.component';
import { WhoweareComponent } from './whoweare/whoweare.component';
import { OtpforgotComponent } from './otpforgot/otpforgot.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { WishlistitemComponent } from './pages/wishlistitem/wishlistitem.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomepageComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'homepage',
    component: HomepageComponent,
  },
  {
    path: 'allcampaigns',
    component: AllcampaignsComponent,
  },
  {
    path: 'productdetail/:id',
    component: ProductdetailComponent,
  },
  {
    path: 'cartpage',
    component: CartpageComponent,
  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'verfication',
    component: VerficationComponent,
  },
  {
    path: 'forgetpassword',
    component: ForgetpasswordComponent,
  },
  {
    path: 'payment',
    component: PaymentComponent,
  },
  {
    path: 'paynow',
    component: PaynowComponent,
  },
  {
    path: 'profile',
    component: ProfileComponent,
  },
  {
    path: 'profile/:value',
    component: ProfileComponent,
  },
  {
    path: 'orderdetail',
    component: OrderdetailComponent
  },
  {
    path: 'otp',
    component: OtpComponent
  },
  {
    path: 'productdetailticket',
    component: ProductdetailticketComponent
  },
  {
    path: 'contactus',
    component: ContactusComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'aboutus',
    component: AboutusComponent
  },
  {
    path: 'termscondition',
    component: TermsconditionComponent
  },
  {
    path: 'privacypolicy',
    component: PrivacypolicyComponent
  },
  {
    path: 'whoweare',
    component: WhoweareComponent
  },
  {
    path: 'howwework',
    component: HowweworkComponent
  },
  {
    path: 'otpforgot',
    component: OtpforgotComponent
  },
  {
    path: 'resetpassword',
    component: ResetpasswordComponent
  },
  {
    path: 'whislisitem',
    component: WishlistitemComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
