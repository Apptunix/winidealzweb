import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api/api.service';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {
  aboutUs: any;

  constructor(private service: ApiService) { }

  ngOnInit() {
    window.scroll(0, 0);
    this.getContactusdata()
  }
  getContactusdata() {
    this.service.getStaticData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
      this.aboutUs=res.data.aboutUs.data
      }
    }
    )
  }
}
