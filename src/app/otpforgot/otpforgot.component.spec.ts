import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpforgotComponent } from './otpforgot.component';

describe('OtpforgotComponent', () => {
  let component: OtpforgotComponent;
  let fixture: ComponentFixture<OtpforgotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpforgotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtpforgotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
