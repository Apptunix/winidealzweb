import { Injectable } from '@angular/core';
// import {NgxSpinnerService} from 'ngx-spinner';
// import {ToastrManager} from 'ng6-toastr-notifications';
import {ApiService} from '../api/api.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ProductDialogComponent } from '../../modal/product-dialog/product-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  dropSetting = {
    enableCheckAll: false,
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  singleDropSetting = {
    enableCheckAll: false,
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };


  constructor(
    // private spinner: NgxSpinnerService,
    // private toaster: ToastrManager,
    private dialog : MatDialog,
    private api: ApiService
  ) { }
  
  // showSpinner() {
  //   this.spinner.show();
  // }
  // hideSpinner() {
  //   this.spinner.hide();
  // }
  // successToast(message) {
  //   this.toaster.successToastr(message, '', {
  //     maxShown: 1
  //   });
  // }
  // errorToast(message) {
  //   this.toaster.errorToastr(message);
  // }


}
