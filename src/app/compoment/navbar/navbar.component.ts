import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  firstName: any;
  lastName: any;
  profile: any = '';
  authToken: any;
  cardItem: any;
  constructor(private router: Router,public comm: CommonService,public service: ApiService) { }

  ngOnInit() {
    $('.toggle_btn').click(function () {
      $('.top_navpages').addClass('add_toppages');
      $('.top_navpages').css('display', 'block');
    });
    if (localStorage.getItem('sigUp')) {
      this.authToken = localStorage.getItem('auth_token')
      this.comm.getProfile().subscribe((result: any) => {   
        this.firstName = JSON.parse(localStorage.getItem('sigUp')).first_name
        this.lastName = JSON.parse(localStorage.getItem('sigUp')).last_name
        this.profile = JSON.parse(localStorage.getItem('sigUp')).image
      });   
    }
  
    if (localStorage.getItem('auth_token')) {
      this.comm.getAuthCardValue().subscribe((result: any) => { (      
          this.comm.campaignslength = result
          );
      });    
    }
    else{
      this.comm.getCardValue().subscribe((result: any) => {
        (this.comm.campaignslength = result);
      });
    }
  }
  goToallcampaigns() {
    this.router.navigate(['allcampaigns'])
  };
  goTohomepage() {
    this.router.navigate(['homepage'])
  };
  goTologin() {
    this.router.navigate(['login'])
  };
  goToprofile() {
    this.router.navigate(['profile'])
  };
  gotowhislisitem() {
    this.router.navigate(['whislisitem'])
  };
  goTohowwework() {
    this.router.navigate(['howwework'])
  };
  goTowhoweare() {
    this.router.navigate(['whoweare'])
  };
  goToSignup(){
    this.router.navigate(['signup'])

  }
  goToCart(){
    this.router.navigate(['cartpage'])
  }
   getUserCart() {
    this.service.getCartData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.cardItem = res.data.cart    
       }
    }
    )
  }
}
