import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api/api.service'
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  constructor(private router: Router,public service:ApiService, private toastr:ToastrService) { }
  email_phone:any;
  password:any
  rememberMe:boolean=false;

    ngOnInit() {
      if (localStorage.getItem("rememberMe")) {
        let rememberMe = JSON.parse(localStorage.getItem('rememberMe'));
        this.rememberMe = true
        this.email_phone = rememberMe.email_phone
        this.password = rememberMe.password
    }
    else{
      this.email_phone = ''
      this.password = ''
      this.rememberMe=false
    }
  }  
  goTosignup() {
    this.router.navigate(['/signup'])
  };
  goToforgetpassword() {
    this.router.navigate(['/forgetpassword'])
  };
  goTohomepage() {
    this.router.navigate(['/homepage'])
  }
  Login(form){
    // if(form.value.rememberMe==true){
    //   var data = {
    //     email_phone: form.value.email_phone,
    //     password: form.value.password
    //   }
    //   sessionStorage.setItem("rememberMe",JSON.stringify(data));
    //  }
    // else { sessionStorage.setItem('rememberMe', '') }
    let Data = new FormData();
    for (let key in form.value) {
      Data.append(`${key}`, form.value[key]);
      console.log(Data,`${key}`)
    }
    if (form.valid) {
      if (this.rememberMe) {
        localStorage.setItem(
          "rememberMe",
          JSON.stringify(form.value)
        );
      } else {
        //localStorage.clear();
      }
      this.service.login(Data).subscribe((res: any) => {
        if (res.status== 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            localStorage.setItem("sigUp", JSON.stringify(res.data));
            localStorage.setItem("auth_token",res.data.auth_token);
            this.router.navigate(['/'])
            if (localStorage.getItem('campaign'))
            console.log(localStorage.getItem('campaign'))
            let data={'campaign':
            JSON.parse(localStorage.getItem('campaign'))
            }
              this.service.AddGuestUserCard(data).subscribe((res: any) => {
                if (res.status== 1) {
                }
              })
              if(localStorage.getItem('fav')){
                 let data1={'campaign':
                 
            JSON.parse(localStorage.getItem('fav'))
            }
                this.service.AddFavguestUser(data1).subscribe((res: any) => {
                  if (res.status== 1) {
                    console.log(res)
                   localStorage.removeItem("fav");

                  }
                })
                
              }
          } else {
            this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          }
        }
        );
      }
  }
}
