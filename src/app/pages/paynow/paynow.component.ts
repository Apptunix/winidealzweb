import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-paynow',
  templateUrl: './paynow.component.html',
  styleUrls: ['./paynow.component.scss']
})
export class PaynowComponent implements OnInit {
  paymentData:any
  cardData: any;
  price: number=0;
  isSubmitted = false;
  shipping_charges: any;
  tax_percentage: any;
  total: any=0;
  campign: any;
  donate_status: any;
  hq_status: any;
  promo_code_name: any= "";
  promo_code_status: any="";
  discount_percent: any;
  city: any='';
  postal_code: any='';
  bulding_name: any='';
  appartment: any='';
  order_number: any='';
  order_name: any='';
  @ViewChild('confirmation', {static: false}) confirmation;
  constructor(private router: Router , public service: ApiService, private route: ActivatedRoute,private toastr: ToastrService) { }

  ngOnInit() {
    this.getSetting()
    $('.grand_total_sec button').click(function () {
      $('.order_confirmation_popup').css('display', 'block')
    });
    $('.order_confirmation_text button').click(function () {
      $('.order_confirmation_popup').css('display', 'none')
    });
    if (localStorage.getItem('auth_token')) {
      this.promo_code_status =JSON.parse(localStorage.getItem('promo_code_status'))
      this.promo_code_name =JSON.parse(localStorage.getItem('promo_code_name'))
      this.campign =JSON.parse(localStorage.getItem('campaignData')).campaign
      this.donate_status =JSON.parse(localStorage.getItem('donate_status'))
      this.discount_percent =JSON.parse(localStorage.getItem('discount_percent'))
      if(localStorage.getItem('hq_status'))
      this.hq_status =JSON.parse(localStorage.getItem('hq_status'))
      this.total=localStorage.getItem("total")
      this.getPaymentDetails();
      this.getCartDetails();
    }
  }
  goTohomepage() {
    this.router.navigate(['homepage'])
  }
  redeemPoints(){
    this.service.getRedeemData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });      console.log(res,"==========")
      this.ngOnInit()
      }
      else{
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
    }
    )
  }
  getPaymentDetails()
  {
    this.service.getPaymenet().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
       
        this.paymentData=res.data.wallet_data
      }
    }
    )
}
submit(){
  if(this.hq_status==0 && this.donate_status==0){
    this.order_name =JSON.parse(localStorage.getItem('sigUp')).order_name
    this.order_number =JSON.parse(localStorage.getItem('sigUp')).order_number
    this.appartment =JSON.parse(localStorage.getItem('sigUp')).appartment
    this.bulding_name =JSON.parse(localStorage.getItem('sigUp')).bulding_name
    this.postal_code =JSON.parse(localStorage.getItem('sigUp')).postal_code
    this.city =JSON.parse(localStorage.getItem('sigUp')).city
  }
  let data={
 "campaign":this.campign,
  "donate_status":this.donate_status,
  "hq_status":this.hq_status,
  "order_name":this.order_name,
  "order_number":this.order_number,
  "appartment":this.appartment,
  "bulding_name":this.bulding_name,
  "city":this.city,
  "postal_code": this.postal_code,
  "total_ammount":this.total,
  "shipping_ammount":this.shipping_charges,
  "discount":this.discount_percent,
  "tax":this.tax_percentage,
  "promo_code_status":this.promo_code_status,
  "promo_code_name":this.promo_code_name
  }
 
  this.service.order(data).subscribe((res: any) => {
    console.log(res);
    if (res.status == 1) {
      // this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      localStorage.removeItem("promo_code_status");
     localStorage.removeItem('promo_code_status')
      localStorage.removeItem('campaignData')
      localStorage.removeItem('donate_status')
      localStorage.removeItem('discount_percent')
      if(localStorage.getItem('hq_status'))
     localStorage.removeItem('hq_status')
      localStorage.removeItem("total")
    this.isSubmitted=true
// this.confirmation.nativeElement.className='order_confirmation_popup'
// $('.grand_total_sec button').click(function () {
//   $('.order_confirmation_popup').css('display', 'block')
// });
      // this.router.navigate(['/'])
    }
    else {
      this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
    }
     
  }
  )

}
  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.shipping_charges=res.data.setting.shipping_charges
        this.tax_percentage=res.data.setting.tax_percentage

      }
    }
    )
  }
  getCartDetails() {
    this.service.getCartData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.cardData = res.data.cart
        // this.price
        this.cardData.forEach(element => {
          this.price=this.price + element.campaign.product.price * element.campaign_count 
        });
        // console.log(this.cardData)
        // this.loyalty_can_be = res.data.loyalty_can_be
        // console.log(this.loyalty_can_be)
        // this.comm.setAuthgetCardValue(this.cardData.length);

        localStorage.removeItem("campaign");
        localStorage.removeItem("campaignslength");



        // this.campionsDetails = res.data

      }
    }
    )
  }
}
