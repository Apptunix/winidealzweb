import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {
  countryCode: any = [];
  config: object;
  number: any = '';
  email: any = ''
  countryStatus: boolean = true;
  country_code: any = '+91';
  constructor(private router: Router, public service: ApiService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.getCountryCode().subscribe((res: any) => {
      this.countryCode = res.countryArray
      console.log(this.countryCode.length, res.countryArray.length,)
      this.config = {
        displayKey: "Code", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Code", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.countryCode.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "Code" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };
    });
  }
  goTologin() {
    this.router.navigate(['login'])
  }
  goTootpforgot() {
    this.router.navigate(['/otpforgot'])
  }
  onSubmit(form:NgForm){
    console.log('form',form.value)
    if (this.country_code == '' || this.country_code == undefined )
    return this.toastr.error('Select Country Code')
  if ((form.value.number == '' || form.value.number == undefined)&& (form.value.email==undefined ||form.value.email==''))
    return this.toastr.error('Select email or phone ')   
if(form.value.number){
    var Data = new FormData();
  Data.append('country_code',this.country_code);
    Data.append('number',form.value.number);
}
if(form.value.email){
    var Data = new FormData();
    Data.append('email',form.value.email);
}
  if (form.valid) {
      this.service.ForgotPassword(Data).subscribe((res: any) => {
        if (res.status== 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            localStorage.setItem("forgot", JSON.stringify({ isForgot: true }));
            localStorage.setItem('forgotPassword', JSON.stringify(res.data));
            this.router.navigate(['/otp'])
          } else {
            this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          }
        }
        );
      }
  }
  selectionChanged(event) {
    console.log(event.value)
    if (event.value != undefined) {
      this.countryStatus = true;
      this.country_code = event.value.Code
    }
    else {
      this.countryStatus = false;
      this.country_code = '';
      // this.user.country_code=''

    }
  }
}
