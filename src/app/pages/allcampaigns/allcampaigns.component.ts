import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/common.service';
import * as _ from "lodash";

@Component({
  selector: 'app-allcampaigns',
  templateUrl: './allcampaigns.component.html',
  styleUrls: ['./allcampaigns.component.scss']
})
export class AllcampaignsComponent implements OnInit {
  all_campaigns: any;
  total: any;
  fav: any;
  tax: any;
  favourite: any;
  cardItem: any;
  cartList: any[];
  constructor(private router: Router,public service: ApiService, private toastr: ToastrService, public comm: CommonService) { }

  ngOnInit() {
    if (this.total > 0 || this.total >= 30) {
      $('.product_progress_height').css({ 'height': '40%', 'background-color': '#0db14b	' })
    } else if (this.total > 30 || this.total <= 40) {
      $('.product_progress_height').css({ 'height': '30%', 'background-color': '#fcb83f' })
    } else {
      $('.product_progress_height').css({ 'height': '80%', 'background-color': '#f44336' })
    // var total_pro = 500;
    // var sell_pro = 200;
    // var total = sell_pro / total_pro * 100
    // if (total >= 90) {
    //   $('.product_progress_height').css({ 'height': '100%', 'background-color': '#1ea842' })
    // } else if (total >= 80) {
    //   $('.product_progress_height').css({ 'height': '90%', 'background-color': '#1ea842' })
    // } else if (total >= 70) {
    //   $('.product_progress_height').css({ 'height': '80%', 'background-color': '#1ea842' })
    // } else if (total >= 60) {
    //   $('.product_progress_height').css({ 'height': '70%', 'background-color': '#FCB83F' })
    // } else if (total >= 50) {
    //   $('.product_progress_height').css({ 'height': '60%', 'background-color': '#FCB83F' })
    // } else if (total >= 40) {
    //   $('.product_progress_height').css({ 'height': '50%', 'background-color': '#FCB83F' })
    // } else if (total >= 30) {
    //   $('.product_progress_height').css({ 'height': '40%', 'background-color': '#FCB83F' })
    // } else if (total >= 20) {
    //   $('.product_progress_height').css({ 'height': '30%', 'background-color': '#FCB83F' })
    // } else if (total >= 10) {
    //   $('.product_progress_height').css({ 'height': '20%', 'background-color': '#FCB83F' })
    // } else if (total <= 10) {
    //   $('.product_progress_height').css('height', '0%')
    }
    this.getHome();
    this.getSetting();
    if (localStorage.getItem("campaign")) {
      this.cartList = JSON.parse(localStorage.getItem("campaign"));
    }
  }
  getHome() {
    if (localStorage.getItem('auth_token'))
      this.service.getAuthdata().subscribe((res: any) => {
        if (res.status == 1)
        this.all_campaigns = res.data['all_campaigns'];
        // this.closing_campaigns = res.data['closing_campaigns'];
        // this.products = res.data['products'];
        // this.charity = res.data['charity'];
        this.all_campaigns.forEach(ele => {
          ele.pendingItem = 0;
          ele.isCartAdded = false;
          ele.cartStatus = false;
          var total_pro =ele. inventory;
          var sell_pro = ele.inventory_sold;
          this. total = sell_pro / total_pro * 100
          // this.total= ele.percentage_sold
          console.log(this.total,"total")
          // this.total= ele.percentage_sold
          console.log(this.total,"total")
          // ele.favourite=0;
        })
        if (localStorage.getItem('auth_token')) {
          this.getUserCart();
        }
      }
      )
    else {
      this.service.getdata().subscribe((res: any) => {
        if (res.status == 1)
          // this.top_banner = res.data['top_banner']
        this.all_campaigns = res.data['all_campaigns'];
        // this.closing_campaigns = res.data['closing_campaigns'];
        // this.products = res.data['products'];
        // this.charity = res.data['charity'];
        console.log(res, "data")
        this.all_campaigns.forEach(ele => {
          var id=ele.id
          
         console.log(id,"iddddd")
          ele.pendingItem = 0;
          ele.isCartAdded = false;
          ele.cartStatus = false;
          var total_pro =ele. inventory;
          var sell_pro = ele.inventory_sold;
          this. total = sell_pro / total_pro * 100
          console.log(this.total,"total")
        }) 
          if(localStorage.getItem('fav')){
          var favourite= JSON.parse(localStorage.getItem('fav'))||[];
          if(favourite && favourite.length>0){
            favourite.forEach(ele => {
              this.fav =ele.campaign_id
              var item = this.all_campaigns.find(
                item => item.id === this.fav
              );
              if(item){
                item.favourite=1
              }
            });
          }
        }
        if(localStorage.getItem('campaign')){
          var cartItems= JSON.parse(localStorage.getItem('campaign'))||[];
          if(cartItems && cartItems.length>0){
            var cartStatusId;
            var cartItemCount;
            cartItems.forEach(ele => {
              cartStatusId =ele.campaign_id
              cartItemCount = ele.campaign_count
              var item = this.all_campaigns.find(
                item => item.id === cartStatusId
              );
              if(item){
              item.pendingItem = cartItemCount
                item['cartStatus'] = true;
                item['cartRemoveStatus']= false;
              }
            });
          }
        }
        }
      )
    }
  }
  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.tax = res.data.setting.tax_percentage

      }
    }
    )
  }

  addFav(index, data) {
    console.log(data, "====================")
    var id;
    id = data.id
    if (localStorage.getItem('auth_token')) {
      this.service.AddRemoveFav(id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.ngOnInit();
          console.log(this.favourite, "favourite")

        } else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });

        }
      }
      )
    }
    else {
      let favourites: any;
      favourites = JSON.parse(localStorage.getItem("fav")) || [];
      var item = favourites.findIndex(
        // console.log(item)
        item => item.campaign_id === data.id
      );
      console.log(item, "item")
      if (item > -1) {
        data.favourite = 0
        var favourite = {
          "campaign_id": data.id,
          "favourite": 0,
          "campaign_count": data.pendingItem == 1 ? 0 : data.pendingItem,
        }
        favourites.splice(item, 1)
        console.log(favourite, "favourites====")
        this.toastr.success("Removed from favouite");
      }
      if (item == -1) {
        var favourite = {
          "campaign_id": data.id,
          "favourite": 1,
          "campaign_count": data.pendingItem == 1 ? 0 : data.pendingItem,

        }
        data.favourite = 1
        favourites.push(favourite);
        console.log(favourite, "favourites====")
        this.toastr.success("Add to favouirte");
      }
      console.log(favourite, "favourites====")
      localStorage.setItem("fav", JSON.stringify(favourites));
    }
  }
  goTocartpage(data) {
   data.pendingItem=1;
    data.isCartAdded = true;
    setTimeout(() => {
      data.isCartAdded = !data.isCartAdded;
      data.cartStatus =true;
    }, 1000);
   
    if (localStorage.getItem('auth_token')) {
      console.log(this.cardItem)
      var item = this.cardItem.find(
        item => item.campaign_id === data.id

      );
      if (item) {
        console.log(item.campaign_count, data.max_per_user, data.pendingItem, "dstssss")
        var quantity = item.campaign_count + data.pendingItem;
        if (quantity > data.max_per_user) {
          this.toastr.warning("You can not add more items.");
        }
        else {
          let Data = new FormData();
          Data.append('campaign_id', data.id);
          Data.append('campaign_count', quantity);
          this.service.addToCart(Data).subscribe((res: any) => {
            if (res.status == 1) {
              this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
              console.log(res, "===========")
              this.getUserCart();
              // this.wishlist = res.data
            }
          }
          )
        }


      }
      if (!item) {
        let Data = new FormData();
        Data.append('campaign_id', data.id);
        Data.append('campaign_count', data.pendingItem);
        this.service.addToCart(Data).subscribe((res: any) => {
          if (res.status == 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            console.log(res, "===========")
            // this.wishlist = res.data
            this.getUserCart();
          }
        }
        )
      }

    }
    else {
      console.log(data, "daata")

      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || [];
      var campaign = {
        "campaign_id": data.id,
        "campaign_count": data.pendingItem
      }
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item) {
        var quantity = item.campaign_count + data.pendingItem;
        if (quantity > data.max_per_user) {
          this.toastr.warning("You can not add more items.");
        } else {
          item.campaign_count = quantity;
          this.toastr.success("Deals Added successfully");
        }
      }
      if (!item) {
        product.push(campaign);
        this.toastr.success("Deals Added successfully");
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
    }
  }

  getUserCart() {
    this.service.getCartData().subscribe((res: any) => {
      if (res.status == 1) {
        this.cardItem = res.data.cart
        if(this.cardItem && this.cardItem.length>0){
          var cartStatusId;
          var cartItemCount;
          this.cardItem.forEach(ele => {
            cartStatusId =ele.campaign_id
            cartItemCount = ele.campaign_count
            console.log(this.all_campaigns);
            if(this.all_campaigns && this.all_campaigns.length>0){
              var item = this.all_campaigns.find(
                item => item.id === cartStatusId
              );
              console.log(this.all_campaigns, item,"camppp");
              if(item){
              item.pendingItem = cartItemCount
                item['cartStatus'] = true;
                item['cartRemoveStatus']= false;
              }
            }
          });
        }
        var count = 0;
        for(let item of this.cardItem){
          count = count + item.campaign_count 
        }
        this.comm.setAuthgetCardValue(count);
      }
    }
    )
  }

  decrement(index,data) {
    data.pendingItem--;
    if(data.pendingItem>=1){
      this.changequantity(data);
    }else{
      data.cartStatus = false;
      data.cartRemoveStatus= true;
      data.isCartAdded = true;
      setTimeout(() => {
      data.cartRemoveStatus = !data.cartRemoveStatus;
      data.isCartAdded = false;
      }, 1000);
      data.pendingItem = 0;
      this.removeItem(index,data)
    }
  }

  removeItem(index, item){
    this.cancel(index,item)
  }
  cancel(index, data) {
    if (localStorage.getItem('auth_token')) {
      this.service.removeFromCart(data.id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      })
    }
    else {
      if (localStorage.getItem("campaign")) {
        this.cartList = JSON.parse(localStorage.getItem("campaign"));
      }
      const i = _.findIndex(this.cartList, {
        campaign_id: data.id,
      });
      if (i > -1) {
        this.cartList.splice(i, 1);
        const data: any = localStorage.setItem(
          "campaign",
          JSON.stringify(this.cartList)
        );
        var count = 0;
        for(let item of this.cartList){
          count = count + item.campaign_count 
        }
        localStorage.setItem(
          "campaignslength",
          JSON.stringify(count)
        );
        this.comm.setgetCardValue();
      }
    }
  }

  increment(data) {
    data.pendingItem++;
     this.changequantity(data);
  }

  changequantity(data) {
    if (localStorage.getItem('auth_token')) {
      var campaign = {
        "campaign_id": data.id,
        "campaign_count": data.pendingItem
      }
      let Data = new FormData();
      Data.append('campaign_id', data.id);
      Data.append('campaign_count', data.pendingItem);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
      }
      )
    } else{
      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || []
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item){
        item.campaign_id = item.campaign_id,
        item.campaign_count =  data.pendingItem
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
    }
  }

  goToproductdetail(data) {
    console.log(data)
    var id;
    id = data.id
    this.router.navigate(['productdetail', id])
  }
}
