import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { get } from 'jquery';
import {  CommonService} from '../../services/common.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  confirm_password: any;
  authToken: string;
  first_name: any;
  last_name: any;
  profile: any;
  url: string;
  file;
  user: any;
  country_code: any;
  number: any;
  countryCode: any;
  loyalty: any;
  Loyalty:any;
  order_name: any;
  order_number: any;
  ticketData: any;
  total: any;
  noTicket:any
  onload: boolean = false;
  country_code_address: any;
  country: any;

  constructor(public comm: CommonService,private route: ActivatedRoute,private router: Router, public service: ApiService, private toastr: ToastrService) { }
  confirmError: boolean
  new_password: any
  current_password: any
  config1: {};
  referral_code:any
  building_name:any
  city:any
  apartment:any;
  postal_code:any
  config: any = {
    id: "page",
    currentPage: 1,
    itemsPerPage: 15
  };
  currentPage: Number;
  gender: any = ['Male', 'Female', 'Other'];
  selectedNationality: string ='';
  nationality: any= [];
  countryStatus: boolean = true;
  countryStatusAddress: boolean = true;
  selectedGender: string;

  ngOnInit() {
    this.currentPage = 1;
    window.scroll(0, 0);
    this.route.params.subscribe(params => {
    var value = params['value']
    if(value){
      $('#referfriend-tab').click();
    }
    });
    this.service.geNationality().subscribe((res:any) => {
      this.nationality = res;
    })
    if (localStorage.getItem('sigUp')) {
      this.order_name =JSON.parse(localStorage.getItem('sigUp')).order_name
      this.order_number =JSON.parse(localStorage.getItem('sigUp')).order_number
      this.postal_code =JSON.parse(localStorage.getItem('sigUp')).postal_code
      this.city =JSON.parse(localStorage.getItem('sigUp')).city
      this.apartment =JSON.parse(localStorage.getItem('sigUp')).apartment
      this.building_name =JSON.parse(localStorage.getItem('sigUp')).building_name
      this.country =JSON.parse(localStorage.getItem('sigUp')).country
      this.selectedNationality =JSON.parse(localStorage.getItem('sigUp')).nationality
      this.selectedGender =JSON.parse(localStorage.getItem('sigUp')).gender
      this.authToken = localStorage.getItem('auth_token')
      this.referral_code= JSON.parse(localStorage.getItem('sigUp')).referral_code
      if (JSON.parse(localStorage.getItem('sigUp')).image != "")
        this.url = JSON.parse(localStorage.getItem('sigUp')).image
      if(this.selectedNationality == ''){
        this.selectedNationality = "United Arab Emirates";
      }
    }
    this.service.getCountryCode().subscribe((res: any) => {
      this.countryCode = res.countryArray
      console.log(this.countryCode.length, res.countryArray.length)
      this.config1 = {
        displayKey: "Code", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Code", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.countryCode.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "Code" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };
    });
    this.getUserData();
    this.getSetting();
    this.getTicket();
    this.getTicketScreen(this.currentPage);
  }
  getUserData() {
    this.service.getUserDetail().subscribe((res: any) => {
      if (res.status == 1) {
        this.user = res.data;
        console.log(this.user)
        this.url = this.url 
        this.first_name = this.user.first_name
        this.last_name = this.user.last_name
        this.number = this.user.number
        this.country_code_address = this.user.country_code
        console.log(res, "res")
      }
    })
  }
  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.loyalty = res.data.loyalty
        console.log(this.loyalty,"loyality")
      }
    }
    )
  }
   getTicketScreen(page) {
    this.service.getTicketData(page).subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
       this.ticketData=res.data.data
       this.total=res.data.total
       this.onload = true;
       this.config = {
         id: "page",
         currentPage: page,
         itemsPerPage: 15,
         totalItems:res.data.total
       };
      }
    }
    )
  }
  pageChange(newPage: number) {
  this.getTicketScreen(newPage)
    window.scroll(0, 0);
  }
  getTicket() {
    this.service.getloyalityData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.loyalty = res.data
      }
    }
    )
  }
  gotohome(){
    this.router.navigate(['/homepage'])
  }
  updateAddress(form:NgForm){
    console.log(form,"datatat")
    let Data = new FormData();
    for (let key in form.value) {
      Data.append(`${key}`, form.value[key]);
      console.log(Data, `${key}`)
    }
    this.service.addAddress(Data).subscribe((res: any) => {
      if (res.status == 1) {
        console.log(res)
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        localStorage.setItem("sigUp", JSON.stringify(res.data));

        this.router.navigate(['/profile'])
      }
      else {
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
    });
  }

  goToorderdetail() {
    this.router.navigate(['orderdetail'])
  }
  redeemPoints(){
    this.service.getRedeemData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });      console.log(res,"==========")
      this.ngOnInit()
      }
      else{
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
    }
    )
  }
  selectionChanged(event) {
    console.log(event.value,"======")
    if (event.value !=undefined) {
     this.countryStatus = true;
     this.country_code = event.value.Code
    }
    else
    {
      this.countryStatus = false;
      this.country_code=''
      this.user.country_code=''
    }
    
  }
  update(f1) {
    console.log(f1.value.country_code,this.country_code,f1.value)
    if (f1.value.country_code == '' || f1.value.country_code== undefined)
      return this.toastr.error('Select Country Code')
    console.log(f1)
    // if (f1.value.number == this.number && f1.value.country_code == this.user.country_code) {
      this.updateProfile(f1);
    //   console.log(f1.value.number,f1.value.country_code)
    // }
    // else{
    //   console.log(f1)
    // }
  }
  updateProfile(form: NgForm){
    let formData = new FormData();
    for (let key in form.value) {
      formData.append(`${key}`, form.value[key]);
      console.log(formData, `${key}`)
    }
    formData.append('image',this.file)
   if (form.valid) {
    console.log("inside formdata"+JSON.stringify(formData));
    this.service.profileUpdate(formData).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          localStorage.setItem("sigUp", JSON.stringify(res.data));
          this.comm.setProfile(res.data);
          this.ngOnInit()
          // this.router.navigate(['/'])
        } 
        if(res.status == 4){
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        localStorage.setItem("updateProfile", JSON.stringify(res.data));
        this.router.navigate(['/otp'])
        }
        else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      }
    );
  }

  }
  goTologout() {
    this.service.logout().subscribe((res: any) => {
      if (res.status == 1) {
        var rememberMe = localStorage.getItem('rememberMe');
        localStorage.clear();
        localStorage.setItem(
          "rememberMe",
          rememberMe
        );
        this.router.navigate(['/']);
      }
    })
  }
  uploadProfileImage(event) {
    this.file = event.target.files[0];
    console.log("evec", event)
    if (event.target.files[0].type.indexOf("image/") == 0) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;

      }
    }
    else {
      this.toastr.warning('Invalid Image', "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });

    }

  }

  goToproductdetailticket() {
    this.router.navigate(['productdetailticket'])
  }

  onPasswordSubmit(form: NgForm) {
    if (this.new_password != this.confirm_password) {
      this.confirmError = true
    } else {
      this.confirmError = false
      let Data = new FormData();
      for (let key in form.value) {
        Data.append(`${key}`, form.value[key]);
        console.log(Data, `${key}`)
      }
      this.service.changePassword(Data).subscribe((res: any) => {
        if (res.status == 1) {
          console.log(res)
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          form.reset();
          this.router.navigate(['/'])
        }
        else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      });
    }
  }

  selectionChangedAddress(event) {
    if (event.value !=undefined) {
     this.countryStatusAddress = true;
     this.country_code_address = event.value.Code
    }
    else
    {
      this.countryStatusAddress = false;
      this.country_code_address=''
    }
    
  }
}
