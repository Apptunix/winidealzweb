import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  goToallcampaigns() {
    this.router.navigate(['allcampaigns'])
  };
  goTofaq() {
    this.router.navigate(['faq'])
  };
  goTocontactus() {
    this.router.navigate(['contactus'])
  };
  goTotermscondition() {
    this.router.navigate(['termscondition'])
  };
  goToaboutus() {
    this.router.navigate(['aboutus'])
  };
  goToprivacypolicy() {
    this.router.navigate(['privacypolicy'])
  };
  goTohowwework() {
    this.router.navigate(['howwework'])
  };
  goTowhoweare() {
    this.router.navigate(['whoweare'])
  };
}
