import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api/api.service';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.scss']
})
export class PrivacypolicyComponent implements OnInit {

  constructor(private service: ApiService) { }
  ngOnInit() {
    window.scroll(0, 0);
    this.getContactusdata()
  }
  getContactusdata() {
    this.service.getStaticData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {

      }
    }
    )
  }
}
